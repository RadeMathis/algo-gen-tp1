#!/bin/python3
import subprocess
import sys
import random

# problem parameter
NB_GENES = 10

# algo parameters
NB_INDIVIDUS = 100   # how many people in the population
PROBA_MUT = .1       # chance to have a mutation
STRAT_MUT = "local"  # can be global, local or mixed

MIXED_PROBA = .2


# find a genome able to create this phenotype
def pheno2geno(pheno):
    output = []
    i = 0
    for char in pheno:
        val = ord(char)
        if val >= ord('0') and val <= ord('9'):
            output.append(val - ord('0'))
        elif val >= ord('A') and val <= ord('Z'):
            output.append(val - ord('A') + 10)
        elif val == ord('_'):
            output.append(36)
    return output

# express the genom of an individual


def geno2pheno(geno):
    output = ""
    for gen in geno:
        i = abs(gen) % 37
        if i >= 0 and i <= 9:
            output += chr(i + ord('0'))
        elif i >= 10 and i <= 35:
            output += chr(i + ord('A') - 10)
        elif i == 36:
            output += '_'
    return output

# express the genom of a population


def geno2pheno_p(pop):
    out = []
    for i in pop:
        out.append(geno2pheno(i))
    return out


def compute_fitness(mdp):
    """
    Renvoi un tabeau de fitness

    Cette fonction va pour chaque mot de passe passé en paramètre,
    calculer la fitness en utilisant l'executable ibi_2017-2018_fitness_linux.
    Si un élément est mauvais (pas le bon nombre de charactère et/ou des
    charactères invalides), la fitness de cet individu est mis directement à 0

    Parameters
    ----------
    arg1 : [str]
        Un tableau de mot de passe sous forme de chaine de charactère

    Returns
    -------
    [float]
        Retourne un tableau de fitness, dans le même ordre que les mots de
        passe

    """
    fitnesses = []
    args = ("./ibi_2017-2018_fitness_linux", "8")
    for i in mdp:
        args += (i,)
    popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    popen.wait()
    output = popen.stdout.read().decode("UTF-8")
    for line in output.splitlines():
        elements = line.split("\t")

        if len(elements) >= 2:
            fitnesses.append(float(elements[1]))
        else:
            fitnesses.append(float(0))
    return fitnesses

# generate a random populaton


def population_aleatoire(nb_individus):
    output = []
    for i in range(nb_individus):
        output.append(individu_aleatoire())
    return output

# generate a random individual


def individu_aleatoire():
    output = []
    for i in range(10):
        output.append(random.randint(0, (2**31) - 1))
    return output

# display a whole population


def afficher_population(population):
    for individu in population:
        print(geno2pheno(individu))

# simple mutration of an individu : one or zero mutation


def mutation(individu):
    if (random.uniform(0., 1.) < PROBA_MUT):
        aleat = random.randint(0, 9)
        e = mutate_gen(individu[aleat])
        new_individu = []
        for i in individu:
            new_individu.append(i)
        new_individu[aleat] = e
        return new_individu
    return individu

# simple mputation of a gene


def mutate_gen(gene):
    if STRAT_MUT == "global":
        return mutate_gen_global(gene)
    elif STRAT_MUT == "local":
        return mutate_gen_local(gene)
    elif STRAT_MUT == "mixed":
        if random.uniform(0., 1.) < MIXED_PROBA:
            return mutate_gen_global(gene)
        else:
            return mutate_gen_local(gene)
    else:
        print("unknown strat {}".format(STRAT_MUT))
        exit(0)

# mutation is fully random on a bit


def mutate_gen_global(gene):
    mask = 2 ** random.randint(0, 30)
    if ((int(gene / mask)) % 2) == 0:
        gene += mask
    else:
        gene -= mask
    return gene

# mutation take a lettre close in the alphabet


def mutate_gen_local(gene):
    return gene + random.choice([-1, 1])


def normalize(fitnesses):
    min_fitness = min(fitnesses)
    for i in range(len(fitnesses)):
        fitnesses[i] -= min_fitness
    somme_fitness = sum(fitnesses)
    if (somme_fitness == 0):
        for i in range(len(fitnesses)):
            fitnesses[i] += 1.
        somme_fitness = len(fitnesses)
    for i in range(len(fitnesses)):
        fitnesses[i] = (fitnesses[i] / somme_fitness) + fitnesses[i - 1]
    return fitnesses


def evolution_ponderee_lineaire(old_population, fitnesses, mutation):
    fitnesses = normalize(fitnesses)
    new_population = []
    for i in range(NB_INDIVIDUS):
        if mutation == "only-mute":
            new_population.append(simple_mute(old_population, fitnesses))
        elif mutation == "copulez":
            new_population.append(rEpRoDuCe(old_population, fitnesses))
    return new_population

# take an individual, mutate it, no reproduction


def simple_mute(population, fitness):
    aleat = random.uniform(0., 1.)
    j = 0
    while fitnesses[j] <= aleat:
        j += 1
    return mutation(population[j])

# ```this has been censored, please prove you are more that 18yo```


def rEpRoDuCe(population, fitnesses):
    alea1 = random.uniform(0., 1.)
    alea2 = random.uniform(0., 1.)
    parent1 = None
    parent2 = None
    i = 0
    while (parent1 is None or parent2 is None):
        if parent1 is None and fitnesses[i] >= alea1:
            parent1 = population[i]
        if parent2 is None and fitnesses[i] >= alea2:
            parent2 = population[i]
        i += 1
    output = []
    for i in range(NB_GENES):
        alea = random.choice([True, False])
        # sorry Carlos, here comes the shitstorm :-/
        output.append((parent1 if alea else parent2)[i])
    return mutation(output)

# evolve a population with a specified strategy


def iterate(population, fitnesses, strategie_name):
    if (strategie_name == "strat-1"):
        return evolution_ponderee_lineaire(population, fitnesses, "only-mute")
    elif (strategie_name == "strat-co"):
        return evolution_ponderee_lineaire(population, fitnesses, "copulez")


# main routine, programme starts here
if __name__ == '__main__':
    population = population_aleatoire(NB_INDIVIDUS)

    etape = 0
    fitnesses = compute_fitness(geno2pheno_p(population))
    while (max(fitnesses) < 1.):
        etape += 1
        max_fitness = max(fitnesses)
        avg_fitness = sum(fitnesses) / float(len(fitnesses))
        # if (etape % 200) == 1 :
        print(end="\r")
        for i in range(79):
            print(end=" ")
        print("\retape : {}\tmax fitness : {}\tavg fitness : {}"
              .format(etape, max_fitness, avg_fitness), end="")

        #population = iterate (population, fitnesses, "strat-1")
        population = iterate(population, fitnesses, "strat-co")

        fitnesses = compute_fitness(geno2pheno_p(population))
    for i in range(len(fitnesses)):
        if fitnesses[i] == 1:
            print("\n", geno2pheno(population[i]))
            break
