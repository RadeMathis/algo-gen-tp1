import subprocess
from algo_gen import *


def c (i) :
    if i >= 0 and i <= 9:
        output = chr(i + ord('0'))
    elif i >= 10 and i <= 35:
        output = chr(i + ord('A') - 10)
    elif i == 36:
        output = '_'
    return output

if __name__ == "__main__" :
    mdps = []
    for i in range (37) :
        mdps.append ("_3RM4H63R" + c(i))
    print (compute_fitness (mdps))
